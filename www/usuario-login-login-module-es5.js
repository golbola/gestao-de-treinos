(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["usuario-login-login-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/usuario/login/login.page.html":
    /*!*************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/usuario/login/login.page.html ***!
      \*************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppUsuarioLoginLoginPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <h1 class=\"ion-text-center\">Login</h1>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n  <div id=\"criar-conta\">\n    <p><ion-button [routerLink]=\"['usuario/new']\" fill=\"clear\" color=\"medium\">Registre-se</ion-button></p>\n  </div>\n\n  <div id=\"logo-div\">\n    <ion-text>\n      <img src=\"../../../assets/icon/iconGym.png\" />\n      <h1>Gym Workout</h1>\n    </ion-text>\n  </div>\n  \n  <form id=\"form-div\" [formGroup]=\"loginForm\" (submit)=\"login()\">\n    <ion-item>\n      <ion-input formControlName=\"email\" type=\"email\" placeholder=\"E-mail\" autofocus=\"true\">\n        <ion-icon name=\"mail\" slot=\"start\"></ion-icon>\n      </ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-input formControlName=\"password\" type=\"password\" placeholder=\"Senha\">\n        <ion-icon name=\"lock-closed\" slot=\"start\"></ion-icon>\n      </ion-input>\n    </ion-item>\n\n    <ion-button  [routerLink]=\"['']\" fill=\"clear\" color=\"medium\">Esqueci minha senha.</ion-button>\n\n    <ion-button [disabled]=\"loginForm.invalid\" type=\"submit\" expand=\"block\" fill=\"clear\">Entrar</ion-button>\n  </form>\n\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/usuario/login/login-routing.module.ts":
    /*!*******************************************************!*\
      !*** ./src/app/usuario/login/login-routing.module.ts ***!
      \*******************************************************/

    /*! exports provided: LoginPageRoutingModule */

    /***/
    function srcAppUsuarioLoginLoginRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPageRoutingModule", function () {
        return LoginPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _login_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./login.page */
      "./src/app/usuario/login/login.page.ts");

      var routes = [{
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_3__["LoginPage"]
      }];

      var LoginPageRoutingModule = function LoginPageRoutingModule() {
        _classCallCheck(this, LoginPageRoutingModule);
      };

      LoginPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], LoginPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/usuario/login/login.module.ts":
    /*!***********************************************!*\
      !*** ./src/app/usuario/login/login.module.ts ***!
      \***********************************************/

    /*! exports provided: LoginPageModule */

    /***/
    function srcAppUsuarioLoginLoginModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPageModule", function () {
        return LoginPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _login_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./login-routing.module */
      "./src/app/usuario/login/login-routing.module.ts");
      /* harmony import */


      var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./login.page */
      "./src/app/usuario/login/login.page.ts");

      var LoginPageModule = function LoginPageModule() {
        _classCallCheck(this, LoginPageModule);
      };

      LoginPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _login_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginPageRoutingModule"]],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
      })], LoginPageModule);
      /***/
    },

    /***/
    "./src/app/usuario/login/login.page.scss":
    /*!***********************************************!*\
      !*** ./src/app/usuario/login/login.page.scss ***!
      \***********************************************/

    /*! exports provided: default */

    /***/
    function srcAppUsuarioLoginLoginPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-toolbar {\n  text-align: center;\n  color: black;\n  --ion-toolbar-background: rgb(110, 86, 218);\n}\n\nion-content {\n  --ion-background-color: linear-gradient(180deg, rgb(110, 86, 218) 20%, rgba(255,255,255,1) 100%);\n  --ion-background-color-rgb: rgb(110, 86, 218);\n}\n\n#criar-conta {\n  color: #e0dddd;\n  text-align: right;\n  margin-right: 1rem;\n  font-size: 1.2rem;\n  font-weight: bold;\n}\n\n#logo-div {\n  height: 40%;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  font-weight: bold;\n}\n\n#logo-div img {\n  width: 60%;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n\n#logo-div h1 {\n  font-weight: bold;\n  text-align: center;\n}\n\n#form-div {\n  padding: 1rem;\n  align-items: center;\n}\n\n#form-div ion-item {\n  align-items: center;\n  font-size: 1.2rem;\n  padding: 0.5rem;\n}\n\n#form-div ion-icon {\n  font-size: 1.5rem;\n  padding: 0.5rem;\n  color: #6e56da;\n}\n\n#form-div p {\n  text-align: right;\n  margin-right: 0.5rem;\n  color: #6e56da;\n}\n\n#form-div ion-button {\n  font-size: 1.5rem;\n  color: #6e56da;\n  font-weight: bold;\n  padding: 0.5rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXN1YXJpby9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBQTtFQUNBLFlBQUE7RUFDQSwyQ0FBQTtBQUNKOztBQUVBO0VBQ0ksZ0dBQUE7RUFDQSw2Q0FBQTtBQUNKOztBQUVBO0VBQ0ksY0FBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0FBQ0o7O0FBRUE7RUFDSSxXQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxpQkFBQTtBQUNKOztBQUNJO0VBQ0ksVUFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBQ1I7O0FBRUk7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0FBQVI7O0FBSUE7RUFFSSxhQUFBO0VBQ0EsbUJBQUE7QUFGSjs7QUFJSTtFQUNJLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0FBRlI7O0FBSUk7RUFDSSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBRlI7O0FBS0k7RUFDSSxpQkFBQTtFQUNBLG9CQUFBO0VBQ0EsY0FBQTtBQUhSOztBQU1JO0VBQ0ksaUJBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0FBSlIiLCJmaWxlIjoic3JjL2FwcC91c3VhcmlvL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi10b29sYmFye1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gICAgLS1pb24tdG9vbGJhci1iYWNrZ3JvdW5kOiByZ2IoMTEwLCA4NiwgMjE4KTtcclxufVxyXG5cclxuaW9uLWNvbnRlbnR7XHJcbiAgICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiBsaW5lYXItZ3JhZGllbnQoMTgwZGVnLCAgcmdiKDExMCwgODYsIDIxOCkgMjAlLCByZ2JhKDI1NSwyNTUsMjU1LDEpIDEwMCUpO1xyXG4gICAgLS1pb24tYmFja2dyb3VuZC1jb2xvci1yZ2I6IHJnYigxMTAsIDg2LCAyMTgpO1xyXG59XHJcblxyXG4jY3JpYXItY29udGF7XHJcbiAgICBjb2xvcjogIHJnYigyMjQsIDIyMSwgMjIxKTtcclxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAxcmVtO1xyXG4gICAgZm9udC1zaXplOiAxLjJyZW07XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxufVxyXG5cclxuI2xvZ28tZGl2IHtcclxuICAgIGhlaWdodDogNDAlO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7ICAgIC8vdmVydGljYWxcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyOyAvL2hvcml6b250YWxcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG5cclxuICAgIGltZ3tcclxuICAgICAgICB3aWR0aDogNjAlO1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogYXV0bzsgXHJcbiAgICB9XHJcblxyXG4gICAgaDF7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgfVxyXG59XHJcblxyXG4jZm9ybS1kaXZ7XHJcbiAgICBcclxuICAgIHBhZGRpbmc6IDFyZW07XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyOyAgXHJcbiAgICBcclxuICAgIGlvbi1pdGVte1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7IFxyXG4gICAgICAgIGZvbnQtc2l6ZTogMS4ycmVtO1xyXG4gICAgICAgIHBhZGRpbmc6IDAuNXJlbTtcclxuICAgIH1cclxuICAgIGlvbi1pY29ue1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMS41cmVtO1xyXG4gICAgICAgIHBhZGRpbmc6IDAuNXJlbTtcclxuICAgICAgICBjb2xvcjogIHJnYigxMTAsIDg2LCAyMTgpO1xyXG4gICAgfVxyXG5cclxuICAgIHB7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAwLjVyZW07XHJcbiAgICAgICAgY29sb3I6ICByZ2IoMTEwLCA4NiwgMjE4KTtcclxuICAgIH1cclxuXHJcbiAgICBpb24tYnV0dG9ue1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMS41cmVtO1xyXG4gICAgICAgIGNvbG9yOiByZ2IoMTEwLCA4NiwgMjE4KTtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBwYWRkaW5nOiAwLjVyZW07XHJcbiAgICB9XHJcbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/usuario/login/login.page.ts":
    /*!*********************************************!*\
      !*** ./src/app/usuario/login/login.page.ts ***!
      \*********************************************/

    /*! exports provided: LoginPage */

    /***/
    function srcAppUsuarioLoginLoginPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPage", function () {
        return LoginPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _shared_usuario_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../shared/usuario.service */
      "./src/app/usuario/shared/usuario.service.ts");

      var LoginPage = /*#__PURE__*/function () {
        function LoginPage(builder, usuarioService) {
          _classCallCheck(this, LoginPage);

          this.builder = builder;
          this.usuarioService = usuarioService;
        }

        _createClass(LoginPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.loginForm = this.builder.group({
              email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email]],
              password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
            });
          }
        }, {
          key: "login",
          value: function login() {//this.usuarioService.login();
          }
        }]);

        return LoginPage;
      }();

      LoginPage.ctorParameters = function () {
        return [{
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]
        }, {
          type: _shared_usuario_service__WEBPACK_IMPORTED_MODULE_3__["UsuarioService"]
        }];
      };

      LoginPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./login.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/usuario/login/login.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./login.page.scss */
        "./src/app/usuario/login/login.page.scss"))["default"]]
      })], LoginPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=usuario-login-login-module-es5.js.map
(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["treino-treino-form-treino-form-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/treino/treino-form/treino-form.page.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/treino/treino-form/treino-form.page.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>treino-form</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n");

/***/ }),

/***/ "./src/app/treino/treino-form/treino-form-routing.module.ts":
/*!******************************************************************!*\
  !*** ./src/app/treino/treino-form/treino-form-routing.module.ts ***!
  \******************************************************************/
/*! exports provided: TreinoFormPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TreinoFormPageRoutingModule", function() { return TreinoFormPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _treino_form_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./treino-form.page */ "./src/app/treino/treino-form/treino-form.page.ts");




const routes = [
    {
        path: '',
        component: _treino_form_page__WEBPACK_IMPORTED_MODULE_3__["TreinoFormPage"]
    }
];
let TreinoFormPageRoutingModule = class TreinoFormPageRoutingModule {
};
TreinoFormPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], TreinoFormPageRoutingModule);



/***/ }),

/***/ "./src/app/treino/treino-form/treino-form.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/treino/treino-form/treino-form.module.ts ***!
  \**********************************************************/
/*! exports provided: TreinoFormPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TreinoFormPageModule", function() { return TreinoFormPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _treino_form_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./treino-form-routing.module */ "./src/app/treino/treino-form/treino-form-routing.module.ts");
/* harmony import */ var _treino_form_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./treino-form.page */ "./src/app/treino/treino-form/treino-form.page.ts");







let TreinoFormPageModule = class TreinoFormPageModule {
};
TreinoFormPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _treino_form_routing_module__WEBPACK_IMPORTED_MODULE_5__["TreinoFormPageRoutingModule"]
        ],
        declarations: [_treino_form_page__WEBPACK_IMPORTED_MODULE_6__["TreinoFormPage"]]
    })
], TreinoFormPageModule);



/***/ }),

/***/ "./src/app/treino/treino-form/treino-form.page.scss":
/*!**********************************************************!*\
  !*** ./src/app/treino/treino-form/treino-form.page.scss ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RyZWluby90cmVpbm8tZm9ybS90cmVpbm8tZm9ybS5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/treino/treino-form/treino-form.page.ts":
/*!********************************************************!*\
  !*** ./src/app/treino/treino-form/treino-form.page.ts ***!
  \********************************************************/
/*! exports provided: TreinoFormPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TreinoFormPage", function() { return TreinoFormPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let TreinoFormPage = class TreinoFormPage {
    constructor() { }
    ngOnInit() {
    }
};
TreinoFormPage.ctorParameters = () => [];
TreinoFormPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-treino-form',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./treino-form.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/treino/treino-form/treino-form.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./treino-form.page.scss */ "./src/app/treino/treino-form/treino-form.page.scss")).default]
    })
], TreinoFormPage);



/***/ })

}]);
//# sourceMappingURL=treino-treino-form-treino-form-module-es2015.js.map
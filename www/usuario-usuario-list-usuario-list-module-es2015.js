(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["usuario-usuario-list-usuario-list-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/usuario/usuario-list/usuario-list.page.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/usuario/usuario-list/usuario-list.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>Usuários</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-searchbar placeholder=\"Pesquisar\"\n    (inClear)=\"doSearchClear()\"\n    (inChange)=\"doSearchClearChange($event)\">\n  </ion-searchbar>\n\n  <ion-list>\n    <ion-item-sliding *ngFor=\"let usuario of usuarios\">\n      <ion-item>\n        <ion-label>{{usuario.name}}</ion-label>\n      </ion-item>\n\n      <ion-item-options side=\"start\">\n\n        <ion-item-option [routerLink]=\"['edit', usuario.id]\">\n          <ion-icon slot=\"icon-only\" name=\"create\" color=\"danger\"></ion-icon> \n        </ion-item-option>\n\n        <ion-item-option (click)=\"delete(usuario)\">\n          <ion-icon slot=\"icon-only\" name=\"trash\" color=\"danger\"></ion-icon> \n        </ion-item-option>\n\n      </ion-item-options>\n\n    </ion-item-sliding>\n  </ion-list>\n\n  <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n    <ion-fab-button color=\"secondary\" [routerLink]=\"['new']\">\n      <ion-icon name=\"add\"></ion-icon>\n    </ion-fab-button>\n  </ion-fab>\n\n</ion-content>");

/***/ }),

/***/ "./src/app/usuario/usuario-list/usuario-list-routing.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/usuario/usuario-list/usuario-list-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: UsuarioListPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsuarioListPageRoutingModule", function() { return UsuarioListPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _usuario_list_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./usuario-list.page */ "./src/app/usuario/usuario-list/usuario-list.page.ts");




const routes = [
    {
        path: '',
        component: _usuario_list_page__WEBPACK_IMPORTED_MODULE_3__["UsuarioListPage"]
    }
];
let UsuarioListPageRoutingModule = class UsuarioListPageRoutingModule {
};
UsuarioListPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], UsuarioListPageRoutingModule);



/***/ }),

/***/ "./src/app/usuario/usuario-list/usuario-list.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/usuario/usuario-list/usuario-list.module.ts ***!
  \*************************************************************/
/*! exports provided: UsuarioListPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsuarioListPageModule", function() { return UsuarioListPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _usuario_list_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./usuario-list-routing.module */ "./src/app/usuario/usuario-list/usuario-list-routing.module.ts");
/* harmony import */ var _usuario_list_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./usuario-list.page */ "./src/app/usuario/usuario-list/usuario-list.page.ts");







let UsuarioListPageModule = class UsuarioListPageModule {
};
UsuarioListPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _usuario_list_routing_module__WEBPACK_IMPORTED_MODULE_5__["UsuarioListPageRoutingModule"]
        ],
        declarations: [_usuario_list_page__WEBPACK_IMPORTED_MODULE_6__["UsuarioListPage"]]
    })
], UsuarioListPageModule);



/***/ }),

/***/ "./src/app/usuario/usuario-list/usuario-list.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/usuario/usuario-list/usuario-list.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzdWFyaW8vdXN1YXJpby1saXN0L3VzdWFyaW8tbGlzdC5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/usuario/usuario-list/usuario-list.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/usuario/usuario-list/usuario-list.page.ts ***!
  \***********************************************************/
/*! exports provided: UsuarioListPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsuarioListPage", function() { return UsuarioListPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _shared_usuario_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared/usuario.service */ "./src/app/usuario/shared/usuario.service.ts");




let UsuarioListPage = class UsuarioListPage {
    constructor(usuarioService, toastCtrl, alertCtrl) {
        this.usuarioService = usuarioService;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.usuarios = [];
    }
    ngOnInit() {
    }
    ionViewWillEnter() {
        this.loadUsuarios();
    }
    /*
    async loadUsuarios(idAcademia?: number, tipo?: number){
      if(tipo == 1){
        this.usuarios = await this.usuarioService.getByAllMembros(idAcademia);
      }else if(tipo == 1){
        this.usuarios = await this.usuarioService.getByAllTipo('aluno');
      }
    }
    */
    loadUsuarios() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.usuarios = yield this.usuarioService.getAll();
        });
    }
    doSearchClear() {
        this.loadUsuarios();
    }
    doSearchClearChange($event) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const value = $event.target.value;
            if (value && value.length >= 2) {
                this.usuarios = yield this.usuarioService.filter(value);
            }
        });
    }
    delete(usuario) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertCtrl.create({
                header: 'Deletar?',
                message: 'Deseja excluir o contato: ${name}?',
                buttons: [{
                        text: 'Cancelar',
                        role: 'cancelar',
                    },
                    {
                        text: 'Excluir',
                        handler: () => {
                            this.executeDelete(usuario);
                        }
                    }]
            });
            yield alert.present();
        });
    }
    executeDelete(usuario) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            try {
                yield this.usuarioService.delete(usuario.idUsuario);
                const index = this.usuarios.indexOf(usuario);
                this.usuarios.slice(index, 1);
                const toast = yield this.toastCtrl.create({
                    header: 'Sucesso',
                    message: 'Contato excluído com sucesso!',
                    color: 'success',
                    position: 'bottom',
                    duration: 3000
                });
                toast.present();
            }
            catch (error) {
                const toast = yield this.toastCtrl.create({
                    header: 'Erro',
                    message: 'Erro ao excluir!',
                    color: 'danger',
                    position: 'bottom',
                    duration: 3000
                });
            }
        });
    }
};
UsuarioListPage.ctorParameters = () => [
    { type: _shared_usuario_service__WEBPACK_IMPORTED_MODULE_3__["UsuarioService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] }
];
UsuarioListPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-usuario-list',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./usuario-list.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/usuario/usuario-list/usuario-list.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./usuario-list.page.scss */ "./src/app/usuario/usuario-list/usuario-list.page.scss")).default]
    })
], UsuarioListPage);



/***/ })

}]);
//# sourceMappingURL=usuario-usuario-list-usuario-list-module-es2015.js.map
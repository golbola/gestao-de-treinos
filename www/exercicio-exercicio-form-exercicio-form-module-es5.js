(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["exercicio-exercicio-form-exercicio-form-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/exercicio/exercicio-form/exercicio-form.page.html":
    /*!*********************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/exercicio/exercicio-form/exercicio-form.page.html ***!
      \*********************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppExercicioExercicioFormExercicioFormPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-title>exercicio-form</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/exercicio/exercicio-form/exercicio-form-routing.module.ts":
    /*!***************************************************************************!*\
      !*** ./src/app/exercicio/exercicio-form/exercicio-form-routing.module.ts ***!
      \***************************************************************************/

    /*! exports provided: ExercicioFormPageRoutingModule */

    /***/
    function srcAppExercicioExercicioFormExercicioFormRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ExercicioFormPageRoutingModule", function () {
        return ExercicioFormPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _exercicio_form_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./exercicio-form.page */
      "./src/app/exercicio/exercicio-form/exercicio-form.page.ts");

      var routes = [{
        path: '',
        component: _exercicio_form_page__WEBPACK_IMPORTED_MODULE_3__["ExercicioFormPage"]
      }];

      var ExercicioFormPageRoutingModule = function ExercicioFormPageRoutingModule() {
        _classCallCheck(this, ExercicioFormPageRoutingModule);
      };

      ExercicioFormPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ExercicioFormPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/exercicio/exercicio-form/exercicio-form.module.ts":
    /*!*******************************************************************!*\
      !*** ./src/app/exercicio/exercicio-form/exercicio-form.module.ts ***!
      \*******************************************************************/

    /*! exports provided: ExercicioFormPageModule */

    /***/
    function srcAppExercicioExercicioFormExercicioFormModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ExercicioFormPageModule", function () {
        return ExercicioFormPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _exercicio_form_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./exercicio-form-routing.module */
      "./src/app/exercicio/exercicio-form/exercicio-form-routing.module.ts");
      /* harmony import */


      var _exercicio_form_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./exercicio-form.page */
      "./src/app/exercicio/exercicio-form/exercicio-form.page.ts");

      var ExercicioFormPageModule = function ExercicioFormPageModule() {
        _classCallCheck(this, ExercicioFormPageModule);
      };

      ExercicioFormPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _exercicio_form_routing_module__WEBPACK_IMPORTED_MODULE_5__["ExercicioFormPageRoutingModule"]],
        declarations: [_exercicio_form_page__WEBPACK_IMPORTED_MODULE_6__["ExercicioFormPage"]]
      })], ExercicioFormPageModule);
      /***/
    },

    /***/
    "./src/app/exercicio/exercicio-form/exercicio-form.page.scss":
    /*!*******************************************************************!*\
      !*** ./src/app/exercicio/exercicio-form/exercicio-form.page.scss ***!
      \*******************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppExercicioExercicioFormExercicioFormPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2V4ZXJjaWNpby9leGVyY2ljaW8tZm9ybS9leGVyY2ljaW8tZm9ybS5wYWdlLnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/exercicio/exercicio-form/exercicio-form.page.ts":
    /*!*****************************************************************!*\
      !*** ./src/app/exercicio/exercicio-form/exercicio-form.page.ts ***!
      \*****************************************************************/

    /*! exports provided: ExercicioFormPage */

    /***/
    function srcAppExercicioExercicioFormExercicioFormPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ExercicioFormPage", function () {
        return ExercicioFormPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var ExercicioFormPage = /*#__PURE__*/function () {
        function ExercicioFormPage() {
          _classCallCheck(this, ExercicioFormPage);
        }

        _createClass(ExercicioFormPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return ExercicioFormPage;
      }();

      ExercicioFormPage.ctorParameters = function () {
        return [];
      };

      ExercicioFormPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-exercicio-form',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./exercicio-form.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/exercicio/exercicio-form/exercicio-form.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./exercicio-form.page.scss */
        "./src/app/exercicio/exercicio-form/exercicio-form.page.scss"))["default"]]
      })], ExercicioFormPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=exercicio-exercicio-form-exercicio-form-module-es5.js.map
(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["usuario-usuario-form-usuario-form-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/usuario/usuario-form/usuario-form.page.html":
    /*!***************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/usuario/usuario-form/usuario-form.page.html ***!
      \***************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppUsuarioUsuarioFormUsuarioFormPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n    <ion-title>{{title}}</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding\">\n  <form (ngSubmit)=\"onSubmit()\">\n    <ion-item>\n      <ion-label position=\"stacked\">Nome</ion-label>\n      <ion-input auocapitalize=\"on\" [(ngModel)]=\"usuario.name\" name=\"name\"></ion-input>\n    </ion-item>\n    <div class=\"ion-margin-vertical\">\n      <ion-button type=\"submit\" color=\"secondary\" expand=\"block\">Salvar</ion-button>\n    </div>\n  </form>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/usuario/usuario-form/usuario-form-routing.module.ts":
    /*!*********************************************************************!*\
      !*** ./src/app/usuario/usuario-form/usuario-form-routing.module.ts ***!
      \*********************************************************************/

    /*! exports provided: UsuarioFormPageRoutingModule */

    /***/
    function srcAppUsuarioUsuarioFormUsuarioFormRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UsuarioFormPageRoutingModule", function () {
        return UsuarioFormPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _usuario_form_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./usuario-form.page */
      "./src/app/usuario/usuario-form/usuario-form.page.ts");

      var routes = [{
        path: '',
        component: _usuario_form_page__WEBPACK_IMPORTED_MODULE_3__["UsuarioFormPage"]
      }];

      var UsuarioFormPageRoutingModule = function UsuarioFormPageRoutingModule() {
        _classCallCheck(this, UsuarioFormPageRoutingModule);
      };

      UsuarioFormPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], UsuarioFormPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/usuario/usuario-form/usuario-form.module.ts":
    /*!*************************************************************!*\
      !*** ./src/app/usuario/usuario-form/usuario-form.module.ts ***!
      \*************************************************************/

    /*! exports provided: UsuarioFormPageModule */

    /***/
    function srcAppUsuarioUsuarioFormUsuarioFormModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UsuarioFormPageModule", function () {
        return UsuarioFormPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _usuario_form_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./usuario-form-routing.module */
      "./src/app/usuario/usuario-form/usuario-form-routing.module.ts");
      /* harmony import */


      var _usuario_form_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./usuario-form.page */
      "./src/app/usuario/usuario-form/usuario-form.page.ts");

      var UsuarioFormPageModule = function UsuarioFormPageModule() {
        _classCallCheck(this, UsuarioFormPageModule);
      };

      UsuarioFormPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _usuario_form_routing_module__WEBPACK_IMPORTED_MODULE_5__["UsuarioFormPageRoutingModule"]],
        declarations: [_usuario_form_page__WEBPACK_IMPORTED_MODULE_6__["UsuarioFormPage"]]
      })], UsuarioFormPageModule);
      /***/
    },

    /***/
    "./src/app/usuario/usuario-form/usuario-form.page.scss":
    /*!*************************************************************!*\
      !*** ./src/app/usuario/usuario-form/usuario-form.page.scss ***!
      \*************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppUsuarioUsuarioFormUsuarioFormPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzdWFyaW8vdXN1YXJpby1mb3JtL3VzdWFyaW8tZm9ybS5wYWdlLnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/usuario/usuario-form/usuario-form.page.ts":
    /*!***********************************************************!*\
      !*** ./src/app/usuario/usuario-form/usuario-form.page.ts ***!
      \***********************************************************/

    /*! exports provided: UsuarioFormPage */

    /***/
    function srcAppUsuarioUsuarioFormUsuarioFormPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UsuarioFormPage", function () {
        return UsuarioFormPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _shared_usuario__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../shared/usuario */
      "./src/app/usuario/shared/usuario.ts");
      /* harmony import */


      var _shared_usuario_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../shared/usuario.service */
      "./src/app/usuario/shared/usuario.service.ts");

      var UsuarioFormPage = /*#__PURE__*/function () {
        function UsuarioFormPage(usuarioService, route, toastCtrl) {
          _classCallCheck(this, UsuarioFormPage);

          this.usuarioService = usuarioService;
          this.route = route;
          this.toastCtrl = toastCtrl;
          this.title = 'Novo contato';
        }

        _createClass(UsuarioFormPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.usuario = new _shared_usuario__WEBPACK_IMPORTED_MODULE_4__["Usuario"]();
            var idParam = this.route.snapshot.paramMap.get('id');

            if (idParam) {
              this.title = 'Editar contato';
              this.loadUsuario(parseInt(idParam));
            }
          }
        }, {
          key: "loadUsuario",
          value: function loadUsuario(id) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.usuarioService.getById(id);

                    case 2:
                      this.usuario = _context.sent;

                    case 3:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "onSubmit",
          value: function onSubmit() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var result, toast, _toast;

              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.prev = 0;
                      _context2.next = 3;
                      return this.usuarioService.save(this.usuario);

                    case 3:
                      result = _context2.sent;
                      this.usuario.idUsuario = result.insertId;
                      _context2.next = 7;
                      return this.toastCtrl.create({
                        header: 'Sucesso',
                        message: 'Contato salvo com sucesso.',
                        color: 'success',
                        position: 'bottom',
                        duration: 3000
                      });

                    case 7:
                      toast = _context2.sent;
                      toast.present();
                      _context2.next = 16;
                      break;

                    case 11:
                      _context2.prev = 11;
                      _context2.t0 = _context2["catch"](0);
                      _context2.next = 15;
                      return this.toastCtrl.create({
                        header: 'Erro',
                        message: 'Contato não salvo!',
                        color: 'danger',
                        position: 'bottom',
                        duration: 3000
                      });

                    case 15:
                      _toast = _context2.sent;

                    case 16:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this, [[0, 11]]);
            }));
          }
        }]);

        return UsuarioFormPage;
      }();

      UsuarioFormPage.ctorParameters = function () {
        return [{
          type: _shared_usuario_service__WEBPACK_IMPORTED_MODULE_5__["UsuarioService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]
        }];
      };

      UsuarioFormPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-usuario-form',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./usuario-form.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/usuario/usuario-form/usuario-form.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./usuario-form.page.scss */
        "./src/app/usuario/usuario-form/usuario-form.page.scss"))["default"]]
      })], UsuarioFormPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=usuario-usuario-form-usuario-form-module-es5.js.map
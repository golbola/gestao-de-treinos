(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["usuario-usuario-list-usuario-list-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/usuario/usuario-list/usuario-list.page.html":
    /*!***************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/usuario/usuario-list/usuario-list.page.html ***!
      \***************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppUsuarioUsuarioListUsuarioListPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-title>Usuários</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-searchbar placeholder=\"Pesquisar\"\n    (inClear)=\"doSearchClear()\"\n    (inChange)=\"doSearchClearChange($event)\">\n  </ion-searchbar>\n\n  <ion-list>\n    <ion-item-sliding *ngFor=\"let usuario of usuarios\">\n      <ion-item>\n        <ion-label>{{usuario.name}}</ion-label>\n      </ion-item>\n\n      <ion-item-options side=\"start\">\n\n        <ion-item-option [routerLink]=\"['edit', usuario.id]\">\n          <ion-icon slot=\"icon-only\" name=\"create\" color=\"danger\"></ion-icon> \n        </ion-item-option>\n\n        <ion-item-option (click)=\"delete(usuario)\">\n          <ion-icon slot=\"icon-only\" name=\"trash\" color=\"danger\"></ion-icon> \n        </ion-item-option>\n\n      </ion-item-options>\n\n    </ion-item-sliding>\n  </ion-list>\n\n  <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n    <ion-fab-button color=\"secondary\" [routerLink]=\"['new']\">\n      <ion-icon name=\"add\"></ion-icon>\n    </ion-fab-button>\n  </ion-fab>\n\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/usuario/usuario-list/usuario-list-routing.module.ts":
    /*!*********************************************************************!*\
      !*** ./src/app/usuario/usuario-list/usuario-list-routing.module.ts ***!
      \*********************************************************************/

    /*! exports provided: UsuarioListPageRoutingModule */

    /***/
    function srcAppUsuarioUsuarioListUsuarioListRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UsuarioListPageRoutingModule", function () {
        return UsuarioListPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _usuario_list_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./usuario-list.page */
      "./src/app/usuario/usuario-list/usuario-list.page.ts");

      var routes = [{
        path: '',
        component: _usuario_list_page__WEBPACK_IMPORTED_MODULE_3__["UsuarioListPage"]
      }];

      var UsuarioListPageRoutingModule = function UsuarioListPageRoutingModule() {
        _classCallCheck(this, UsuarioListPageRoutingModule);
      };

      UsuarioListPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], UsuarioListPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/usuario/usuario-list/usuario-list.module.ts":
    /*!*************************************************************!*\
      !*** ./src/app/usuario/usuario-list/usuario-list.module.ts ***!
      \*************************************************************/

    /*! exports provided: UsuarioListPageModule */

    /***/
    function srcAppUsuarioUsuarioListUsuarioListModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UsuarioListPageModule", function () {
        return UsuarioListPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _usuario_list_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./usuario-list-routing.module */
      "./src/app/usuario/usuario-list/usuario-list-routing.module.ts");
      /* harmony import */


      var _usuario_list_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./usuario-list.page */
      "./src/app/usuario/usuario-list/usuario-list.page.ts");

      var UsuarioListPageModule = function UsuarioListPageModule() {
        _classCallCheck(this, UsuarioListPageModule);
      };

      UsuarioListPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _usuario_list_routing_module__WEBPACK_IMPORTED_MODULE_5__["UsuarioListPageRoutingModule"]],
        declarations: [_usuario_list_page__WEBPACK_IMPORTED_MODULE_6__["UsuarioListPage"]]
      })], UsuarioListPageModule);
      /***/
    },

    /***/
    "./src/app/usuario/usuario-list/usuario-list.page.scss":
    /*!*************************************************************!*\
      !*** ./src/app/usuario/usuario-list/usuario-list.page.scss ***!
      \*************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppUsuarioUsuarioListUsuarioListPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzdWFyaW8vdXN1YXJpby1saXN0L3VzdWFyaW8tbGlzdC5wYWdlLnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/usuario/usuario-list/usuario-list.page.ts":
    /*!***********************************************************!*\
      !*** ./src/app/usuario/usuario-list/usuario-list.page.ts ***!
      \***********************************************************/

    /*! exports provided: UsuarioListPage */

    /***/
    function srcAppUsuarioUsuarioListUsuarioListPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UsuarioListPage", function () {
        return UsuarioListPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _shared_usuario_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../shared/usuario.service */
      "./src/app/usuario/shared/usuario.service.ts");

      var UsuarioListPage = /*#__PURE__*/function () {
        function UsuarioListPage(usuarioService, toastCtrl, alertCtrl) {
          _classCallCheck(this, UsuarioListPage);

          this.usuarioService = usuarioService;
          this.toastCtrl = toastCtrl;
          this.alertCtrl = alertCtrl;
          this.usuarios = [];
        }

        _createClass(UsuarioListPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "ionViewWillEnter",
          value: function ionViewWillEnter() {
            this.loadUsuarios();
          }
          /*
          async loadUsuarios(idAcademia?: number, tipo?: number){
            if(tipo == 1){
              this.usuarios = await this.usuarioService.getByAllMembros(idAcademia);
            }else if(tipo == 1){
              this.usuarios = await this.usuarioService.getByAllTipo('aluno');
            }
          }
          */

        }, {
          key: "loadUsuarios",
          value: function loadUsuarios() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.usuarioService.getAll();

                    case 2:
                      this.usuarios = _context.sent;

                    case 3:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "doSearchClear",
          value: function doSearchClear() {
            this.loadUsuarios();
          }
        }, {
          key: "doSearchClearChange",
          value: function doSearchClearChange($event) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var value;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      value = $event.target.value;

                      if (!(value && value.length >= 2)) {
                        _context2.next = 5;
                        break;
                      }

                      _context2.next = 4;
                      return this.usuarioService.filter(value);

                    case 4:
                      this.usuarios = _context2.sent;

                    case 5:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "delete",
          value: function _delete(usuario) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var _this = this;

              var alert;
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      _context3.next = 2;
                      return this.alertCtrl.create({
                        header: 'Deletar?',
                        message: 'Deseja excluir o contato: ${name}?',
                        buttons: [{
                          text: 'Cancelar',
                          role: 'cancelar'
                        }, {
                          text: 'Excluir',
                          handler: function handler() {
                            _this.executeDelete(usuario);
                          }
                        }]
                      });

                    case 2:
                      alert = _context3.sent;
                      _context3.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }, {
          key: "executeDelete",
          value: function executeDelete(usuario) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var index, toast, _toast;

              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      _context4.prev = 0;
                      _context4.next = 3;
                      return this.usuarioService["delete"](usuario.idUsuario);

                    case 3:
                      index = this.usuarios.indexOf(usuario);
                      this.usuarios.slice(index, 1);
                      _context4.next = 7;
                      return this.toastCtrl.create({
                        header: 'Sucesso',
                        message: 'Contato excluído com sucesso!',
                        color: 'success',
                        position: 'bottom',
                        duration: 3000
                      });

                    case 7:
                      toast = _context4.sent;
                      toast.present();
                      _context4.next = 16;
                      break;

                    case 11:
                      _context4.prev = 11;
                      _context4.t0 = _context4["catch"](0);
                      _context4.next = 15;
                      return this.toastCtrl.create({
                        header: 'Erro',
                        message: 'Erro ao excluir!',
                        color: 'danger',
                        position: 'bottom',
                        duration: 3000
                      });

                    case 15:
                      _toast = _context4.sent;

                    case 16:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this, [[0, 11]]);
            }));
          }
        }]);

        return UsuarioListPage;
      }();

      UsuarioListPage.ctorParameters = function () {
        return [{
          type: _shared_usuario_service__WEBPACK_IMPORTED_MODULE_3__["UsuarioService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]
        }];
      };

      UsuarioListPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-usuario-list',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./usuario-list.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/usuario/usuario-list/usuario-list.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./usuario-list.page.scss */
        "./src/app/usuario/usuario-list/usuario-list.page.scss"))["default"]]
      })], UsuarioListPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=usuario-usuario-list-usuario-list-module-es5.js.map
(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["contacts-contact-list-contact-list-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/contacts/contact-list/contact-list.page.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/contacts/contact-list/contact-list.page.html ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>Contatos</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-searchbar placeholder=\"Pesquisar\"\n    (inClear)=\"doSearchClear()\"\n    (inChange)=\"doSearchClearChange($event)\">\n  </ion-searchbar>\n\n  <ion-list>\n    <ion-item-sliding *ngFor=\"let contact of contacts\">\n      <ion-item>\n        <ion-label>{{contact.name}}</ion-label>\n      </ion-item>\n\n      <ion-item-options side=\"start\">\n\n        <ion-item-option [routerLink]=\"['edit', contact.id]\">\n          <ion-icon slot=\"icon-only\" name=\"create\" color=\"danger\"></ion-icon> \n        </ion-item-option>\n\n        <ion-item-option (click)=\"delete(contact)\">\n          <ion-icon slot=\"icon-only\" name=\"trash\" color=\"danger\"></ion-icon> \n        </ion-item-option>\n\n      </ion-item-options>\n\n    </ion-item-sliding>\n  </ion-list>\n\n  <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n    <ion-fab-button color=\"secondary\" [routerLink]=\"['new']\">\n      <ion-icon name=\"add\"></ion-icon>\n    </ion-fab-button>\n  </ion-fab>\n\n</ion-content>\n");

/***/ }),

/***/ "./src/app/contacts/contact-list/contact-list-routing.module.ts":
/*!**********************************************************************!*\
  !*** ./src/app/contacts/contact-list/contact-list-routing.module.ts ***!
  \**********************************************************************/
/*! exports provided: ContactListPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactListPageRoutingModule", function() { return ContactListPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _contact_list_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./contact-list.page */ "./src/app/contacts/contact-list/contact-list.page.ts");




const routes = [
    {
        path: '',
        component: _contact_list_page__WEBPACK_IMPORTED_MODULE_3__["ContactListPage"]
    }
];
let ContactListPageRoutingModule = class ContactListPageRoutingModule {
};
ContactListPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ContactListPageRoutingModule);



/***/ }),

/***/ "./src/app/contacts/contact-list/contact-list.module.ts":
/*!**************************************************************!*\
  !*** ./src/app/contacts/contact-list/contact-list.module.ts ***!
  \**************************************************************/
/*! exports provided: ContactListPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactListPageModule", function() { return ContactListPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _contact_list_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./contact-list-routing.module */ "./src/app/contacts/contact-list/contact-list-routing.module.ts");
/* harmony import */ var _contact_list_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./contact-list.page */ "./src/app/contacts/contact-list/contact-list.page.ts");







let ContactListPageModule = class ContactListPageModule {
};
ContactListPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _contact_list_routing_module__WEBPACK_IMPORTED_MODULE_5__["ContactListPageRoutingModule"]
        ],
        declarations: [_contact_list_page__WEBPACK_IMPORTED_MODULE_6__["ContactListPage"]]
    })
], ContactListPageModule);



/***/ }),

/***/ "./src/app/contacts/contact-list/contact-list.page.scss":
/*!**************************************************************!*\
  !*** ./src/app/contacts/contact-list/contact-list.page.scss ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbnRhY3RzL2NvbnRhY3QtbGlzdC9jb250YWN0LWxpc3QucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/contacts/contact-list/contact-list.page.ts":
/*!************************************************************!*\
  !*** ./src/app/contacts/contact-list/contact-list.page.ts ***!
  \************************************************************/
/*! exports provided: ContactListPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactListPage", function() { return ContactListPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _shared_contact_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared/contact.service */ "./src/app/contacts/shared/contact.service.ts");




let ContactListPage = class ContactListPage {
    constructor(contactService, toastCtrl, alertCtrl) {
        this.contactService = contactService;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.contacts = [];
    }
    ngOnInit() {
    }
    ionViewWillEnter() {
        this.loadContacts();
    }
    loadContacts() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.contacts = yield this.contactService.getAll();
        });
    }
    doSearchClear() {
        this.loadContacts();
    }
    doSearchClearChange($event) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const value = $event.target.value;
            if (value && value.length >= 2) {
                this.contacts = yield this.contactService.filter(value);
            }
        });
    }
    delete(contact) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertCtrl.create({
                header: 'Deletar?',
                message: 'Deseja excluir o contato: ${name}?',
                buttons: [{
                        text: 'Cancelar',
                        role: 'cancelar',
                    },
                    {
                        text: 'Excluir',
                        handler: () => {
                            this.executeDelete(contact);
                        }
                    }]
            });
            yield alert.present();
        });
    }
    executeDelete(contact) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            try {
                yield this.contactService.delete(contact.id);
                const index = this.contacts.indexOf(contact);
                this.contacts.slice(index, 1);
                const toast = yield this.toastCtrl.create({
                    header: 'Sucesso',
                    message: 'Contato excluído com sucesso!',
                    color: 'success',
                    position: 'bottom',
                    duration: 3000
                });
                toast.present();
            }
            catch (error) {
                const toast = yield this.toastCtrl.create({
                    header: 'Erro',
                    message: 'Erro ao excluir!',
                    color: 'danger',
                    position: 'bottom',
                    duration: 3000
                });
            }
        });
    }
};
ContactListPage.ctorParameters = () => [
    { type: _shared_contact_service__WEBPACK_IMPORTED_MODULE_3__["ContactService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] }
];
ContactListPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-contact-list',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./contact-list.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/contacts/contact-list/contact-list.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./contact-list.page.scss */ "./src/app/contacts/contact-list/contact-list.page.scss")).default]
    })
], ContactListPage);



/***/ })

}]);
//# sourceMappingURL=contacts-contact-list-contact-list-module-es2015.js.map
(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["exercicio-exercicio-list-exercicio-list-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/exercicio/exercicio-list/exercicio-list.page.html":
    /*!*********************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/exercicio/exercicio-list/exercicio-list.page.html ***!
      \*********************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppExercicioExercicioListExercicioListPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-title>exercicio-list</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/exercicio/exercicio-list/exercicio-list-routing.module.ts":
    /*!***************************************************************************!*\
      !*** ./src/app/exercicio/exercicio-list/exercicio-list-routing.module.ts ***!
      \***************************************************************************/

    /*! exports provided: ExercicioListPageRoutingModule */

    /***/
    function srcAppExercicioExercicioListExercicioListRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ExercicioListPageRoutingModule", function () {
        return ExercicioListPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _exercicio_list_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./exercicio-list.page */
      "./src/app/exercicio/exercicio-list/exercicio-list.page.ts");

      var routes = [{
        path: '',
        component: _exercicio_list_page__WEBPACK_IMPORTED_MODULE_3__["ExercicioListPage"]
      }];

      var ExercicioListPageRoutingModule = function ExercicioListPageRoutingModule() {
        _classCallCheck(this, ExercicioListPageRoutingModule);
      };

      ExercicioListPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ExercicioListPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/exercicio/exercicio-list/exercicio-list.module.ts":
    /*!*******************************************************************!*\
      !*** ./src/app/exercicio/exercicio-list/exercicio-list.module.ts ***!
      \*******************************************************************/

    /*! exports provided: ExercicioListPageModule */

    /***/
    function srcAppExercicioExercicioListExercicioListModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ExercicioListPageModule", function () {
        return ExercicioListPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _exercicio_list_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./exercicio-list-routing.module */
      "./src/app/exercicio/exercicio-list/exercicio-list-routing.module.ts");
      /* harmony import */


      var _exercicio_list_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./exercicio-list.page */
      "./src/app/exercicio/exercicio-list/exercicio-list.page.ts");

      var ExercicioListPageModule = function ExercicioListPageModule() {
        _classCallCheck(this, ExercicioListPageModule);
      };

      ExercicioListPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _exercicio_list_routing_module__WEBPACK_IMPORTED_MODULE_5__["ExercicioListPageRoutingModule"]],
        declarations: [_exercicio_list_page__WEBPACK_IMPORTED_MODULE_6__["ExercicioListPage"]]
      })], ExercicioListPageModule);
      /***/
    },

    /***/
    "./src/app/exercicio/exercicio-list/exercicio-list.page.scss":
    /*!*******************************************************************!*\
      !*** ./src/app/exercicio/exercicio-list/exercicio-list.page.scss ***!
      \*******************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppExercicioExercicioListExercicioListPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2V4ZXJjaWNpby9leGVyY2ljaW8tbGlzdC9leGVyY2ljaW8tbGlzdC5wYWdlLnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/exercicio/exercicio-list/exercicio-list.page.ts":
    /*!*****************************************************************!*\
      !*** ./src/app/exercicio/exercicio-list/exercicio-list.page.ts ***!
      \*****************************************************************/

    /*! exports provided: ExercicioListPage */

    /***/
    function srcAppExercicioExercicioListExercicioListPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ExercicioListPage", function () {
        return ExercicioListPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var ExercicioListPage = /*#__PURE__*/function () {
        function ExercicioListPage() {
          _classCallCheck(this, ExercicioListPage);
        }

        _createClass(ExercicioListPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return ExercicioListPage;
      }();

      ExercicioListPage.ctorParameters = function () {
        return [];
      };

      ExercicioListPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-exercicio-list',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./exercicio-list.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/exercicio/exercicio-list/exercicio-list.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./exercicio-list.page.scss */
        "./src/app/exercicio/exercicio-list/exercicio-list.page.scss"))["default"]]
      })], ExercicioListPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=exercicio-exercicio-list-exercicio-list-module-es5.js.map
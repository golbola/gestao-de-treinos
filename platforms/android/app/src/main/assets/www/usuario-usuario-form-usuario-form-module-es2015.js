(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["usuario-usuario-form-usuario-form-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/usuario/usuario-form/usuario-form.page.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/usuario/usuario-form/usuario-form.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n    <ion-title>{{title}}</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding\">\n  <form (ngSubmit)=\"onSubmit()\">\n    <ion-item>\n      <ion-label position=\"stacked\">Nome</ion-label>\n      <ion-input auocapitalize=\"on\" [(ngModel)]=\"usuario.name\" name=\"name\"></ion-input>\n    </ion-item>\n    <div class=\"ion-margin-vertical\">\n      <ion-button type=\"submit\" color=\"secondary\" expand=\"block\">Salvar</ion-button>\n    </div>\n  </form>\n</ion-content>");

/***/ }),

/***/ "./src/app/usuario/usuario-form/usuario-form-routing.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/usuario/usuario-form/usuario-form-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: UsuarioFormPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsuarioFormPageRoutingModule", function() { return UsuarioFormPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _usuario_form_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./usuario-form.page */ "./src/app/usuario/usuario-form/usuario-form.page.ts");




const routes = [
    {
        path: '',
        component: _usuario_form_page__WEBPACK_IMPORTED_MODULE_3__["UsuarioFormPage"]
    }
];
let UsuarioFormPageRoutingModule = class UsuarioFormPageRoutingModule {
};
UsuarioFormPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], UsuarioFormPageRoutingModule);



/***/ }),

/***/ "./src/app/usuario/usuario-form/usuario-form.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/usuario/usuario-form/usuario-form.module.ts ***!
  \*************************************************************/
/*! exports provided: UsuarioFormPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsuarioFormPageModule", function() { return UsuarioFormPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _usuario_form_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./usuario-form-routing.module */ "./src/app/usuario/usuario-form/usuario-form-routing.module.ts");
/* harmony import */ var _usuario_form_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./usuario-form.page */ "./src/app/usuario/usuario-form/usuario-form.page.ts");







let UsuarioFormPageModule = class UsuarioFormPageModule {
};
UsuarioFormPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _usuario_form_routing_module__WEBPACK_IMPORTED_MODULE_5__["UsuarioFormPageRoutingModule"]
        ],
        declarations: [_usuario_form_page__WEBPACK_IMPORTED_MODULE_6__["UsuarioFormPage"]]
    })
], UsuarioFormPageModule);



/***/ }),

/***/ "./src/app/usuario/usuario-form/usuario-form.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/usuario/usuario-form/usuario-form.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzdWFyaW8vdXN1YXJpby1mb3JtL3VzdWFyaW8tZm9ybS5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/usuario/usuario-form/usuario-form.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/usuario/usuario-form/usuario-form.page.ts ***!
  \***********************************************************/
/*! exports provided: UsuarioFormPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsuarioFormPage", function() { return UsuarioFormPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _shared_usuario__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../shared/usuario */ "./src/app/usuario/shared/usuario.ts");
/* harmony import */ var _shared_usuario_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../shared/usuario.service */ "./src/app/usuario/shared/usuario.service.ts");






let UsuarioFormPage = class UsuarioFormPage {
    constructor(usuarioService, route, toastCtrl) {
        this.usuarioService = usuarioService;
        this.route = route;
        this.toastCtrl = toastCtrl;
        this.title = 'Novo contato';
    }
    ngOnInit() {
        this.usuario = new _shared_usuario__WEBPACK_IMPORTED_MODULE_4__["Usuario"]();
        const idParam = this.route.snapshot.paramMap.get('id');
        if (idParam) {
            this.title = 'Editar contato';
            this.loadUsuario(parseInt(idParam));
        }
    }
    loadUsuario(id) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.usuario = yield this.usuarioService.getById(id);
        });
    }
    onSubmit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            try {
                const result = yield this.usuarioService.save(this.usuario);
                this.usuario.idUsuario = result.insertId;
                const toast = yield this.toastCtrl.create({
                    header: 'Sucesso',
                    message: 'Contato salvo com sucesso.',
                    color: 'success',
                    position: 'bottom',
                    duration: 3000
                });
                toast.present();
            }
            catch (error) {
                const toast = yield this.toastCtrl.create({
                    header: 'Erro',
                    message: 'Contato não salvo!',
                    color: 'danger',
                    position: 'bottom',
                    duration: 3000
                });
            }
        });
    }
};
UsuarioFormPage.ctorParameters = () => [
    { type: _shared_usuario_service__WEBPACK_IMPORTED_MODULE_5__["UsuarioService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] }
];
UsuarioFormPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-usuario-form',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./usuario-form.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/usuario/usuario-form/usuario-form.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./usuario-form.page.scss */ "./src/app/usuario/usuario-form/usuario-form.page.scss")).default]
    })
], UsuarioFormPage);



/***/ })

}]);
//# sourceMappingURL=usuario-usuario-form-usuario-form-module-es2015.js.map
(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["treino-treino-list-treino-list-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/treino/treino-list/treino-list.page.html":
    /*!************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/treino/treino-list/treino-list.page.html ***!
      \************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppTreinoTreinoListTreinoListPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-title>treino-list</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/treino/treino-list/treino-list-routing.module.ts":
    /*!******************************************************************!*\
      !*** ./src/app/treino/treino-list/treino-list-routing.module.ts ***!
      \******************************************************************/

    /*! exports provided: TreinoListPageRoutingModule */

    /***/
    function srcAppTreinoTreinoListTreinoListRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TreinoListPageRoutingModule", function () {
        return TreinoListPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _treino_list_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./treino-list.page */
      "./src/app/treino/treino-list/treino-list.page.ts");

      var routes = [{
        path: '',
        component: _treino_list_page__WEBPACK_IMPORTED_MODULE_3__["TreinoListPage"]
      }];

      var TreinoListPageRoutingModule = function TreinoListPageRoutingModule() {
        _classCallCheck(this, TreinoListPageRoutingModule);
      };

      TreinoListPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], TreinoListPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/treino/treino-list/treino-list.module.ts":
    /*!**********************************************************!*\
      !*** ./src/app/treino/treino-list/treino-list.module.ts ***!
      \**********************************************************/

    /*! exports provided: TreinoListPageModule */

    /***/
    function srcAppTreinoTreinoListTreinoListModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TreinoListPageModule", function () {
        return TreinoListPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _treino_list_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./treino-list-routing.module */
      "./src/app/treino/treino-list/treino-list-routing.module.ts");
      /* harmony import */


      var _treino_list_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./treino-list.page */
      "./src/app/treino/treino-list/treino-list.page.ts");

      var TreinoListPageModule = function TreinoListPageModule() {
        _classCallCheck(this, TreinoListPageModule);
      };

      TreinoListPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _treino_list_routing_module__WEBPACK_IMPORTED_MODULE_5__["TreinoListPageRoutingModule"]],
        declarations: [_treino_list_page__WEBPACK_IMPORTED_MODULE_6__["TreinoListPage"]]
      })], TreinoListPageModule);
      /***/
    },

    /***/
    "./src/app/treino/treino-list/treino-list.page.scss":
    /*!**********************************************************!*\
      !*** ./src/app/treino/treino-list/treino-list.page.scss ***!
      \**********************************************************/

    /*! exports provided: default */

    /***/
    function srcAppTreinoTreinoListTreinoListPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RyZWluby90cmVpbm8tbGlzdC90cmVpbm8tbGlzdC5wYWdlLnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/treino/treino-list/treino-list.page.ts":
    /*!********************************************************!*\
      !*** ./src/app/treino/treino-list/treino-list.page.ts ***!
      \********************************************************/

    /*! exports provided: TreinoListPage */

    /***/
    function srcAppTreinoTreinoListTreinoListPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TreinoListPage", function () {
        return TreinoListPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var TreinoListPage = /*#__PURE__*/function () {
        function TreinoListPage() {
          _classCallCheck(this, TreinoListPage);
        }

        _createClass(TreinoListPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return TreinoListPage;
      }();

      TreinoListPage.ctorParameters = function () {
        return [];
      };

      TreinoListPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-treino-list',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./treino-list.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/treino/treino-list/treino-list.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./treino-list.page.scss */
        "./src/app/treino/treino-list/treino-list.page.scss"))["default"]]
      })], TreinoListPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=treino-treino-list-treino-list-module-es5.js.map
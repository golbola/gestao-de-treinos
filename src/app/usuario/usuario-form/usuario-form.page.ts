import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Usuario } from '../shared/usuario';
import { UsuarioService } from '../shared/usuario.service';

@Component({
  selector: 'app-usuario-form',
  templateUrl: './usuario-form.page.html',
  styleUrls: ['./usuario-form.page.scss'],
})
export class UsuarioFormPage implements OnInit {

  title: string = 'Novo contato';
  usuario: Usuario;

  constructor(
    private usuarioService: UsuarioService,
    private route: ActivatedRoute,
    private toastCtrl: ToastController
  ) { }

  ngOnInit() {
    this.usuario = new Usuario();

    const idParam = this.route.snapshot.paramMap.get('id');
    if(idParam){
      this.title = 'Editar contato';
      this.loadUsuario(parseInt(idParam));
    }
  }

  async loadUsuario(id: number){
    this.usuario = await this.usuarioService.getById(id);
  }

  async onSubmit(){
    try {
      const result = await this.usuarioService.save(this.usuario);
      this.usuario.idUsuario = result.insertId;

      const toast = await this.toastCtrl.create({
        header: 'Sucesso',
        message: 'Contato salvo com sucesso.',
        color: 'success',
        position: 'bottom',
        duration: 3000
      })

      toast.present();
    } catch (error) {
      const toast = await this.toastCtrl.create({
        header: 'Erro',
        message: 'Contato não salvo!',
        color: 'danger',
        position: 'bottom',
        duration: 3000
      })
    }
  }
}

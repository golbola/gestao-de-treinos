import { Component, OnInit } from '@angular/core';
import { ToastController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { Usuario } from '../shared/usuario';
import { UsuarioService } from '../shared/usuario.service';

@Component({
  selector: 'app-usuario-list',
  templateUrl: './usuario-list.page.html',
  styleUrls: ['./usuario-list.page.scss'],
})


export class UsuarioListPage implements OnInit {

  usuarios: Usuario[] = [];

  constructor(
    private usuarioService: UsuarioService,
    private toastCtrl: ToastController,
    private alertCtrl: AlertController,
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.loadUsuarios();
  }

  /*
  async loadUsuarios(idAcademia?: number, tipo?: number){
    if(tipo == 1){
      this.usuarios = await this.usuarioService.getByAllMembros(idAcademia);
    }else if(tipo == 1){
      this.usuarios = await this.usuarioService.getByAllTipo('aluno');
    }
  }
  */
  async loadUsuarios(){
    this.usuarios = await this.usuarioService.getAll();
  }

  doSearchClear(){
    this.loadUsuarios();
  }
  
  async doSearchClearChange($event: any){
    const value = $event.target.value;
    if(value && value.length >= 2){
      this.usuarios = await this.usuarioService.filter(value);
    }
  }

  async delete(usuario: Usuario){
    const alert = await this.alertCtrl.create({
      header: 'Deletar?',
      message: 'Deseja excluir o contato: ${name}?',
      buttons: [{
        text: 'Cancelar',
        role: 'cancelar',
      },
      {
        text: 'Excluir',
        handler: () => {
          this.executeDelete(usuario);
        }
      }]
    });
  
    await alert.present();
  }

  async executeDelete(usuario:Usuario){
    try {
      
      await this.usuarioService.delete(usuario.idUsuario);

      const index = this.usuarios.indexOf(usuario);
      this.usuarios.slice(index, 1);

      const toast = await this.toastCtrl.create({
        header: 'Sucesso',
        message: 'Contato excluído com sucesso!',
        color: 'success',
        position: 'bottom',
        duration: 3000
      })

      toast.present();
    } catch (error) {
      const toast = await this.toastCtrl.create({
        header: 'Erro',
        message: 'Erro ao excluir!',
        color: 'danger',
        position: 'bottom',
        duration: 3000
      })
    }
  }
}

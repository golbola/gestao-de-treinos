import { Injectable } from '@angular/core';
import { DatabaseService } from 'src/app/core/service/database.service';
import { Usuario } from './usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(
    private db: DatabaseService
  ) { }

  async login(email: String, senha: String){
    const sql = 'select * from usuarios where idUsuario =? and email =?';
    const data = [email, senha];
    const result = await this.db.executeSQL(sql, data);
    const rows = result.rows;
    if(rows && rows.length > 0){
      console.log('Login efetuado com sucesso!');
    }else{ 
      console.log('Login efetuado com sucesso!');
    }
  }

  save(usuario: Usuario){
    if(usuario.idUsuario > 0){
      return this.update(usuario);
    }else{
      return this.insert(usuario);
    }
  }

  saveMembro(usuario: Usuario, idMembro: number){
    if(usuario.idUsuario > 0){
      return false;
    }else{
      return this.insertMembro(usuario, idMembro);
    }
  }

  private insert(usuario: Usuario){
    const sql = "insert into usuarios (name, cpf, email, senha) values (?, ?, ?, ?)";
    const data = [usuario.name, usuario.cpf, usuario.email, usuario.senha];

    return this.db.executeSQL(sql, data);
  }

  private insertMembro(usuario: Usuario, idUsuario: number){
    const sql = "insert into membroAcademia (idAcademia, idUsuario) values (?, ?)";
    const data = [usuario.idUsuario, idUsuario];

    return this.db.executeSQL(sql, data);
  }

  private update(usuario: Usuario){
    const sql = "update usuarios set name = ? where idUsuario = ?";
    const data = [usuario.name, usuario.idUsuario];

    return this.db.executeSQL(sql, data);
  }

  delete(idUsuario: number){
    const sql = 'delete from usuarios where idUsuario = ?';
    const data = [idUsuario];

    return this.db.executeSQL(sql, data);
  }


  async getByAllTipo(tipo: String){
    const sql = 'select * from usuarios where tipo =?';
    const data = [tipo];
    const result = await this.db.executeSQL(sql, data);
    const usuarios = this.fillUsuarios(result.rows);
    return usuarios;
  } 

  async getById(idUsuario: number){
    const sql = 'select * from usuarios where idUsuario =?';
    const data = [idUsuario];
    const result = await this.db.executeSQL(sql, data);
    const rows = result.rows;
    const usuario = new Usuario();
    if(rows && rows.length > 0){
      const item = rows.item(0);
      usuario.idUsuario = item.idUsuario;
      usuario.name = item.name;
    }
    return usuario;
  } 

  async getByAllMembros(idAcademia: number){
    const sql = 'select * from membroAcademia where idAcademia =?';
    const data = [idAcademia];
    const result = await this.db.executeSQL(sql, data);
    const usuarios = this.fillUsuarios(result.rows);
    return usuarios;
  } 

  async getByCpf(cpf: String){
    const sql = 'select * from usuarios where cpf =?';
    const data = [cpf];
    const result = await this.db.executeSQL(sql, data);
    const rows = result.rows;
    const usuario = new Usuario();
    if(rows && rows.length > 0){
      const item = rows.item(0);
      usuario.idUsuario = item.idUsuario;
      usuario.name = item.name;
    }
    return usuario;
  } 

  private fillUsuarios(rows: any){
    const usuarios: Usuario[] = [];

    for (let i = 0; i < rows.length; i++){
      const item = rows.item(i);
      const usuario = new Usuario();
      usuario.idUsuario = item.idUsuario;
      usuario.name = item.name;
      usuarios.push(usuario);
    }

    return usuarios;
  }
  
  async getAll(){
    const sql = 'select * from usuarios';
    const result = await this.db.executeSQL(sql);
    const usuarios = this.fillUsuarios(result.rows);
    return usuarios;
  }

  async filter(text: string){
    const sql = 'select * from usuarios where name like ?';
    const data = ['%${text}%'];
    const result = await this.db.executeSQL(sql, data);
    const usuarios = this.fillUsuarios(result.rows);
    return usuarios;
  }

}
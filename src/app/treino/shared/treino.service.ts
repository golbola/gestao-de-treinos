import { Injectable } from '@angular/core';
import { DatabaseService } from 'src/app/core/service/database.service';
import { Treino } from './treino';

@Injectable({
  providedIn: 'root'
})
export class TreinoService {

  constructor(
    private db: DatabaseService
  ) { }

  save(treino: Treino){
    if(treino.idTreino > 0){
      return this.update(treino);
    }else{
      return this.insert(treino);
    }
  }

  private insert(treino: Treino){
    const sql = "insert into treino (idProfessor, idAluno, dataCriacao) values (?, ?, ?)";
    const data = [treino.idProfessor, treino.idAluno, treino.dataCriacao];

    return this.db.executeSQL(sql, data);
  }

  private update(treino: Treino){
    const sql = "update treinos set idProfessor = ?, idAluno = ? where idTreino = ?";
    const data = [treino.idProfessor, treino.idAluno];

    return this.db.executeSQL(sql, data);
  }

  delete(idTreino: Treino){
    const sql = 'delete from treinos where idTreino = ?';
    const data = [idTreino];

    return this.db.executeSQL(sql, data);
  }

  async getById(idTreino: number){
    const sql = 'select * from treinos where idTreino =?';
    const data = [idTreino];
    const result = await this.db.executeSQL(sql, data);
    const rows = result.rows;
    const treino = new Treino();
    if(rows && rows.length > 0){
      const item = rows.item(0);
      treino.idTreino = item.idTreino;
      treino.idProfessor = item.idProfessor;
      treino.idAluno = item.idAluno;
      treino.dataCriacao = item.dataCriacao;
    }
    return treino;
  }  

  private fillTreinos(rows: any){
    const treinos: Treino[] = [];

    for (let i = 0; i < rows.length; i++){
      const item = rows.item(i);
      const treino = new Treino();
      treino.idTreino = item.idTreino;
      treino.idProfessor= item.name;
      treino.idAluno = item.membroCorporal;
      treinos.push(treino);
    }
    return treinos;
  }
  
  async getAll(){
    const sql = 'select * from treinos';
    const result = await this.db.executeSQL(sql);
    const treinos = this.fillTreinos(result.rows);
    return treinos;
  }

  async filter(text: string){
    const sql = 'select * from treinos where name like ?';
    const data = ['%${text}%'];
    const result = await this.db.executeSQL(sql, data);
    const treinos = this.fillTreinos(result.rows);
    return treinos;
  }
}

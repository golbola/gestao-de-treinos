import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TreinoFormPage } from './treino-form.page';

describe('TreinoFormPage', () => {
  let component: TreinoFormPage;
  let fixture: ComponentFixture<TreinoFormPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreinoFormPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TreinoFormPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

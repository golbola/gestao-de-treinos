import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TreinoFormPageRoutingModule } from './treino-form-routing.module';

import { TreinoFormPage } from './treino-form.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TreinoFormPageRoutingModule
  ],
  declarations: [TreinoFormPage]
})
export class TreinoFormPageModule {}

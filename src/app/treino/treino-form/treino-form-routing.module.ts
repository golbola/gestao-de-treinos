import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TreinoFormPage } from './treino-form.page';

const routes: Routes = [
  {
    path: '',
    component: TreinoFormPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TreinoFormPageRoutingModule {}

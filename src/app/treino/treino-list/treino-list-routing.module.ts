import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TreinoListPage } from './treino-list.page';

const routes: Routes = [
  {
    path: '',
    component: TreinoListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TreinoListPageRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TreinoListPageRoutingModule } from './treino-list-routing.module';

import { TreinoListPage } from './treino-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TreinoListPageRoutingModule
  ],
  declarations: [TreinoListPage]
})
export class TreinoListPageModule {}

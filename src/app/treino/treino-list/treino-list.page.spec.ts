import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TreinoListPage } from './treino-list.page';

describe('TreinoListPage', () => {
  let component: TreinoListPage;
  let fixture: ComponentFixture<TreinoListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreinoListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TreinoListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

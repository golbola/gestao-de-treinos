import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExercicioFormPage } from './exercicio-form.page';

const routes: Routes = [
  {
    path: '',
    component: ExercicioFormPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExercicioFormPageRoutingModule {}

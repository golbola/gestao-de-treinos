import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExercicioFormPageRoutingModule } from './exercicio-form-routing.module';

import { ExercicioFormPage } from './exercicio-form.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExercicioFormPageRoutingModule
  ],
  declarations: [ExercicioFormPage]
})
export class ExercicioFormPageModule {}

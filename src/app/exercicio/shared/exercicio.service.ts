import { Injectable } from '@angular/core';
import { DatabaseService } from 'src/app/core/service/database.service';
import { Exercicio } from './exercicio';

@Injectable({
  providedIn: 'root'
})
export class ExercicioService {
  constructor(
    private db: DatabaseService
  ) { }

  save(exercicio: Exercicio){
    if(exercicio.idExercicio > 0){
      return this.update(exercicio);
    }else{
      return this.insert(exercicio);
    }
  }

  private insert(exercicio: Exercicio){
    const sql = "insert into exercicio (name, membroCorporal) values (?, ?)";
    const data = [exercicio.name, exercicio.membroCorporal];

    return this.db.executeSQL(sql, data);
  }

  private update(exercicio: Exercicio){
    const sql = "update exercicios set name = ?, membroCorporal = ? where idExercicio = ?";
    const data = [exercicio.name, exercicio.membroCorporal];

    return this.db.executeSQL(sql, data);
  }

  delete(idExercicio: Exercicio){
    const sql = 'delete from exercicios where idExercicio = ?';
    const data = [idExercicio];

    return this.db.executeSQL(sql, data);
  }


  async getByAllTipo(membro: String){
    const sql = 'select * from exercicios where membroCorporal =?';
    const data = [membro];
    const result = await this.db.executeSQL(sql, data);
    const exercicios = this.fillExercicios(result.rows);
    return exercicios;
  } 

  async getById(idExercicio: number){
    const sql = 'select * from exercicios where idExercicio =?';
    const data = [idExercicio];
    const result = await this.db.executeSQL(sql, data);
    const rows = result.rows;
    const exercicio = new Exercicio();
    if(rows && rows.length > 0){
      const item = rows.item(0);
      exercicio.idExercicio = item.idExercicio;
      exercicio.name = item.name;
      exercicio.membroCorporal = item.membroCorporal;
    }
    return exercicio;
  }  

  private fillExercicios(rows: any){
    const exercicios: Exercicio[] = [];

    for (let i = 0; i < rows.length; i++){
      const item = rows.item(i);
      const exercicio = new Exercicio();
      exercicio.idExercicio = item.idExercicio;
      exercicio.name = item.name;
      exercicio.membroCorporal = item.membroCorporal;
      exercicios.push(exercicio);
    }
    return exercicios;
  }
  
  async getAll(){
    const sql = 'select * from exercicios';
    const result = await this.db.executeSQL(sql);
    const exercicios = this.fillExercicios(result.rows);
    return exercicios;
  }

  async filter(text: string){
    const sql = 'select * from exercicios where name like ?';
    const data = ['%${text}%'];
    const result = await this.db.executeSQL(sql, data);
    const exercicios = this.fillExercicios(result.rows);
    return exercicios;
  }
}

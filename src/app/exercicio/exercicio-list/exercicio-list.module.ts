import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExercicioListPageRoutingModule } from './exercicio-list-routing.module';

import { ExercicioListPage } from './exercicio-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExercicioListPageRoutingModule
  ],
  declarations: [ExercicioListPage]
})
export class ExercicioListPageModule {}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ExercicioListPage } from './exercicio-list.page';

describe('ExercicioListPage', () => {
  let component: ExercicioListPage;
  let fixture: ComponentFixture<ExercicioListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExercicioListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ExercicioListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

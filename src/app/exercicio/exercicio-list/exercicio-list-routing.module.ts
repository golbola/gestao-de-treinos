import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExercicioListPage } from './exercicio-list.page';

const routes: Routes = [
  {
    path: '',
    component: ExercicioListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExercicioListPageRoutingModule {}

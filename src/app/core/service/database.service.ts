import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
@Injectable({
  providedIn: 'root'
})
export class DatabaseService {

  db: SQLiteObject;
  databaseName: string = 'gestaoTreinos.db';

  constructor(
    private sqLite: SQLite, private sqLitePorter: SQLitePorter
  ) { }

  async createDatabase(){
    const sqlCreateDatabase = this.getCreateTable();
    const result = await this.sqLitePorter.importSqlToDb(this.db, sqlCreateDatabase);
    return result ? true : false;
  }

  async openDatabase(){
    try {
      this.db = await this.sqLite.create({ name: this.databaseName, location: 'default' });
      await this.createDatabase();
    } catch (error) {
      console.log('Erro ao abrir o banco', error);
    }
  }

  getCreateTable(){
    const sqls = [];
    sqls.push('CREATE TABLE IF NOT EXISTS usuarios (' +
      'idUsuario integer primary key AUTOINCREMENT, tipo varchar(100), name varchar(100), email varchar(100), cpf varchar(100), senha varchar(100));' +
      'CREATE TABLE IF NOT EXISTS membroAcademia (' + 
      'idMembro integer primary key AUTOINCREMENT, idAcademia integer(100), idUsuario integer(100));' +
      'CREATE TABLE IF NOT EXISTS treino (' + 
      'idTreino integer primary key AUTOINCREMENT, idProfessor integer(100), idAluno integer(100), dataCriacao varchar(100));' + 
      'CREATE TABLE IF NOT EXISTS itemTreino (' +
      'idItemTreino integer primary key AUTOINCREMENT, idTreino integer(100), idExercicio integer(100));' + 
      'CREATE TABLE IF NOT EXISTS exercicios (' +
      'idExercicio integer primary key AUTOINCREMENT, name varchar(100), membroCorporal varchar(100))');
    return sqls.join('\n');
  }

  executeSQL(sql: string, params?: any[]){
    return this.db.executeSql(sql, params);
  }
}
